﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ProductoFactura
    {
        private int id;
        private string sku;
        private string nombre;
        private int cantidad;
        private double precio;

        public ProductoFactura(int id, string sku, string nombre, int cantidad, double precio)
        {
            this.id = id;
            this.sku = sku;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.precio = precio;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }
    }
}
